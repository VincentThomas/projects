import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AuthModule } from './auth/auth.module';
import { RouterModule, Routes } from '@nestjs/core';

const routes: Routes = [
  {
    path: 'auth',
    module: AuthModule,
  },
];

@Module({
  imports: [AuthModule, RouterModule.register(routes)],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
