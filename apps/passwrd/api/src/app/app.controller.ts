import { Controller, Get } from '@nestjs/common';
import { Credential } from '@passwrd/types';

@Controller()
export class AppController {
  constructor() {}

  @Get()
  getData() {}
}
