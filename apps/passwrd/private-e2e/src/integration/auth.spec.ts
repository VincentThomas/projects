import { email, password } from '../fixtures/user.json';

describe('Authentication', () => {
  beforeEach(() => {
    cy.logout();
    cy.visit('/app/auth');
  });
  afterEach(() => cy.logout());

  it('Authenticate using the login page.', () => {
    cy.get('input[type=email]').type(email);
    cy.get('input[type=password]').type(password);
    cy.get('button[type="submit"]').click();

    cy.get('.p-menubar .p-menuitem-link').should('exist').and('be.visible');

    cy.get('div.header-container>h1.title').should('contain', 'Overview');
    cy.get('.btn--multi').should('contain', 'New');

    cy.get('.card--1>h1.title').should('contain', 'Websites');
    cy.get('.card--2>h1.title').should('contain', 'Notes');
    cy.get('.card--3>h1.title').should('contain', 'Contacts');
  });
});
