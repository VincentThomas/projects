describe('Dashboard Home Page', () => {
  beforeEach(() => {
    cy.login();
    cy.visit('/app/dashboard');
  });
  afterEach(() => cy.logout());

  it('Home page', () => {
    cy.get('.p-menubar .p-menuitem-link').should('exist').and('be.visible');

    cy.get('div.header-container>h1.title').should('contain', 'Overview');
    cy.get('.btn--multi').should('contain', 'New');

    cy.get('.btn--multi .p-button-icon-only').click();

    cy.get('.p-menu-overlay').should('be.visible');

    cy.get('.card--1>h1.title').should('contain', 'Websites');
    cy.get('.card--2>h1.title').should('contain', 'Notes');
    cy.get('.card--3>h1.title').should('contain', 'Contacts');
  });
});
