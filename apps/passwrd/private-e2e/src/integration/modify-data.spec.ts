describe('Home Page', () => {
  beforeEach(() => {
    cy.login();
    cy.visit('/app/dashboard');
  });
  afterEach(() => cy.logout());

  it('Authenticate using the login page.', () => {
    cy.get('.btn--multi').should('contain', 'New');
    cy.get('.p-splitbutton-defaultbutton').click();

    cy.get('.p-dialog-title').should('contain', 'Add Credential');
    cy.get('input[type=text]:first').type('TEST_WEBSITE');
    cy.get('input[type=text]').last().type('TEST_USERNAME');
    cy.get('input[type=password]').type('TEST_PASSWORD');
    cy.get('.btn--submit').click();
    cy.get('div.header-container>h1.title').should('contain', 'Overview');

    cy.get('.card--1>h1.title').should('contain', 'Websites');
    cy.get('.card--2>h1.title').should('contain', 'Notes');
    cy.get('.card--3>h1.title').should('contain', 'Contacts');

    cy.get('.p-toast-summary').should('exist');
    cy.get('.p-toast-detail').should('exist');
    cy.visit('/app/dashboard/w');
    cy.contains('Remove').click();
  });
});
