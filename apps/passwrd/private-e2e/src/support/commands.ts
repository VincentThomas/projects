// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Cypress {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  // interface Chainable<Subject> {
  // login(email: string, password: string): void;
  // }
}

// import { signOut } from '@angular/fire/auth';
// import { initializeApp } from 'firebase/app';
// import { getAuth } from 'firebase/auth';

// import * as config from '../fixtures/firebase.config.json';

// const app = initializeApp(config);

// const auth = getAuth(app);
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email: string, password: string) => {
//   console.log('Custom command example: Login', email, password);
// });

import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { attachCustomCommands } from 'cypress-firebase';

const config = {
  apiKey: 'AIzaSyA9Cy_xFlEjma-GRBPOoWOutZkriL-Mw3Q',
  authDomain: 'passwrd-web.firebaseapp.com',
  projectId: 'passwrd-web',
  storageBucket: 'passwrd-web.appspot.com',
  messagingSenderId: '372858537042',
  appId: '1:372858537042:web:0f3d00970b53f6e2e48b8b',
};

firebase.initializeApp(config);

attachCustomCommands({ Cypress, cy, firebase });

//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
