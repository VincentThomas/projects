import * as admin from 'firebase-admin';
import { plugin as cypressFirebasePlugin } from 'cypress-firebase';
export default (on: any, config: any) => {
  on('task', {
    log(message: any) {
      console.log(message);
      return null;
    },
  });

  const extendedConfig = cypressFirebasePlugin(on, config, admin);

  return extendedConfig;
};
