import { ChipModule } from 'primeng/chip';
import { NoteCardComponent } from './note-card.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [NoteCardComponent],
  imports: [CommonModule, ChipModule, ButtonModule],
  exports: [NoteCardComponent],
})
export class NoteCardModule {}
