import { Router } from '@angular/router';
import { Credential } from '../../../../../../../../libs/passwrd/types/src/index';
import { Observable, of } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';
import { StorageService } from '../../../core/services/storage.service';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'ui-dashboard-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly storage: StorageService,
    private readonly auth: AuthService
  ) {}

  @Input('websites') websites$: Observable<Credential[]> = of([]);
  @Input('notes') notes$: Observable<any>;
  @Input('contacts') contacts$: Observable<any>;

  ngOnInit() {
    this.auth.getUser().subscribe(({ uid }: any) => {
      this.websites$ = this.storage.getWebsites(uid);
      this.notes$ = this.storage.getNotes(uid);
    });
  }

  toWebsite() {
    this.router.navigate(['dashboard', 'w']);
  }

  toNotes() {
    this.router.navigate(['dashboard', 'n']);
  }

  toContacts() {}
}
