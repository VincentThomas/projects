import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'smart-website-card',
  templateUrl: './website-card.component.html',
  styleUrls: ['./website-card.component.scss'],
})
export class WebsiteCardComponent implements OnInit {
  @Input('website') data: any;
  @Output('remove') remove: EventEmitter<any> = new EventEmitter();
  @Output('reload') reload: EventEmitter<undefined> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
    console.log(this.data);
  }

  visitWebsite(website: string) {
    console.log(this.data);
    website = website.replace('https://', '').replace('http://', '');
    window.open(`https://${website}`);
  }
  removeWebsite(websites: any) {
    this.remove.emit(websites);
  }
}
