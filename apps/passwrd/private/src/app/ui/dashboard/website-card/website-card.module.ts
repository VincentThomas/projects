import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { WebsiteCardComponent } from './website-card.component';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [WebsiteCardComponent],
  imports: [CommonModule, CardModule, ButtonModule],
  exports: [WebsiteCardComponent],
})
export class WebsiteCardModule {}
