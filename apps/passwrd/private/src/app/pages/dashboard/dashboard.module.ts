import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard.component';
import { ShellModule } from '../../shell/shell.module';
import { SplitButtonModule } from 'primeng/splitbutton';
import { DialogService } from 'primeng/dynamicdialog';
import { NewWebsiteModule } from './components/new-website/new-website.module';
import { NewNoteModule } from './components/new-note/new-note.module';
import { MessageService } from 'primeng/api';
import { HeaderModule } from '../../ui/dashboard/header/header.module';
import { DividerModule } from 'primeng/divider';
import { WebsitesComponent } from './websites/websites.component';
import { ToastModule } from 'primeng/toast';
import { AuthGuard } from '../../core/guards/auth.guard';
import { WebsiteCardModule } from '../../ui/dashboard/website-card/website-card.module';
import { NoDataComponent } from './components/no-data/no-data.component';
import { NotesComponent } from './notes/notes.component';
import { NoteCardModule } from '@app/ui/dashboard/note-card/note-card.module';
import { TableModule } from 'primeng/table';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        data: { title: 'Overview' },
        component: HomeComponent,
      },
      {
        path: 'w',
        data: { title: 'Websites' },
        component: WebsitesComponent,
      },
      {
        path: 'n',
        data: { title: 'Notes' },
        component: NotesComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [
    HomeComponent,
    DashboardComponent,
    WebsitesComponent,
    NoDataComponent,
    NotesComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ShellModule,
    SplitButtonModule,
    NewWebsiteModule,
    NewNoteModule,
    NoteCardModule,
    HeaderModule,
    DividerModule,
    ToastModule,
    // // CoreModule,
    WebsiteCardModule,
    TableModule,
  ],
  providers: [DialogService, MessageService],
  // exports: [RouterModule],
})
export class DashboardModule {}
