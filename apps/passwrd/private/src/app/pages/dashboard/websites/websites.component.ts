import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '@core/auth/auth.service';
import { StorageService } from '@core/services/storage.service';
import { Credential } from '@passwrd/types';

@Component({
  templateUrl: './websites.component.html',
  styleUrls: ['./websites.component.scss'],
})
export class WebsitesComponent implements OnInit {
  websites$: Observable<Credential[]>;

  sub: Subscription;
  sub2: Subscription;

  constructor(
    private readonly auth: AuthService,
    private readonly storage: StorageService
  ) {}

  ngOnInit(): void {
    this.sub = this.auth.getUser().subscribe((user) => {
      this.websites$ = this.storage.getWebsites(user?.uid as string);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    try {
      this.sub2.unsubscribe();
    } catch {}
  }

  removeWebsite(event: any) {
    this.sub2 = this.auth.getUser().subscribe((user: any) => {
      this.storage.removeWebsite(user.uid as string, event.website);
    });
  }
}
