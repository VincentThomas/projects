import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs';
import { DialogService } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';
import { NewWebsiteComponent } from './components/new-website/new-website.component';
import { NewNoteComponent } from './components/new-note/new-note.component';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [MessageService],
})
export class DashboardComponent implements OnInit {
  constructor(
    private readonly route: ActivatedRoute,
    private readonly dialog: DialogService,
    private readonly msg: MessageService
  ) {}

  items = [
    { label: 'Website', icon: 'pi pi-link', command: () => this.newWebsite() },
    { label: 'Secure note', icon: 'pi pi-file', command: () => this.newNote() },
    {
      label: 'Contact',
      disabled: true,
      icon: 'pi pi-user',
      command: () => this.newContact(),
    },
  ];

  ngOnInit(): void {}

  getTitle() {
    const length = this.route.children.length - 1;

    return this.route.children[length]?.data.pipe(
      map((v: any) => v?.['title'])
    );
  }

  new(comp: any, title: string) {
    const ref = this.dialog.open(comp, {
      closable: true,
      closeOnEscape: true,
      header: `Add ${title}`,
      width: '90%',
      style: { 'max-width': '40rem' },
    });
    ref.onClose.subscribe((obj) => {
      if (obj) {
        console.log(obj);
        this.msg.add({
          severity: obj.type,
          summary: obj.title,
          detail: obj.desc,
        });
      }
    });
  }

  newWebsite() {
    const comp = NewWebsiteComponent;
    this.new(comp, 'Credential');
  }

  newNote() {
    const comp = NewNoteComponent;
    this.new(comp, 'Secure Note');
  }

  newContact() {}
}
