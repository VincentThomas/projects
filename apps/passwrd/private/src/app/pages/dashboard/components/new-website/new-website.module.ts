import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { PasswordModule } from 'primeng/password';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { NewWebsiteComponent } from './new-website.component';

@NgModule({
  declarations: [NewWebsiteComponent],
  imports: [
    CommonModule,
    ButtonModule,
    PasswordModule,
    ReactiveFormsModule,
    InputTextModule,
  ],
})
export class NewWebsiteModule {}
