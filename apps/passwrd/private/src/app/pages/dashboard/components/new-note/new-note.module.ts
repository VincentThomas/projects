import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewNoteComponent } from './new-note.component';

import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { ChipsModule } from 'primeng/chips';

import { ButtonModule } from 'primeng/button';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [NewNoteComponent],
  imports: [
    CommonModule,
    InputTextareaModule,
    InputTextModule,
    ButtonModule,
    ReactiveFormsModule,
    ChipsModule,
    FormsModule,
  ],
})
export class NewNoteModule {}
