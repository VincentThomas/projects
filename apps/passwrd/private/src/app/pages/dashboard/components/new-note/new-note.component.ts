import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { map } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StorageService } from '@core/services/storage.service';
import { AuthService } from '@core/auth/auth.service';

@Component({
  templateUrl: './new-note.component.html',
  styleUrls: ['./new-note.component.scss'],
})
export class NewNoteComponent implements OnInit {
  constructor(
    private readonly fb: FormBuilder,
    private readonly storage: StorageService,
    private readonly auth: AuthService,
    public readonly ref: DynamicDialogRef
  ) {}

  form: FormGroup;

  ngOnInit(): void {
    this.form = this.fb.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
  }

  tags: string[];

  goBack() {
    this.ref.close();
  }

  addNote() {
    const { title, description } = this.form.value;
    const tags = this.tags;

    this.auth
      .getUser()
      .pipe(map((v) => v?.uid))
      .subscribe((uid: string | undefined) => {
        this.storage
          .addNote(tags, title, description, uid as string)
          .then(() => {
            this.ref.close({
              type: 'success',
              title: 'Added Website',
              desc: 'Website added to your profile successfully!',
            });
          });
      });
  }
}
