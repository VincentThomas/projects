import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { map } from 'rxjs';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StorageService } from '@core/services/storage.service';
import { AuthService } from '@core/auth/auth.service';

@Component({
  templateUrl: './new-website.component.html',
  styleUrls: ['./new-website.component.scss'],
})
export class NewWebsiteComponent implements OnInit {
  constructor(
    private readonly fb: FormBuilder,
    private readonly storage: StorageService,
    private readonly auth: AuthService,
    public readonly ref: DynamicDialogRef
  ) {}
  website: string;
  username: string;
  password: string;

  @Output('event') output = new EventEmitter<string | undefined>();

  form: FormGroup;

  ngOnInit(): void {
    this.form = this.fb.group({
      website: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  goBack() {
    this.ref.close();
  }

  addWebsite() {
    const { website, username, password } = this.form.value;

    this.auth
      .getUser()
      .pipe(map((v) => v?.uid))
      .subscribe((uid: string | undefined) => {
        console.log(uid);
        this.storage
          .addWebsite(website, username, password, uid as string)
          .then(() => {
            this.ref.close({
              type: 'success',
              title: 'Added Website',
              desc: 'Website added to your profile successfully!',
            });
          });
      });
  }
}
