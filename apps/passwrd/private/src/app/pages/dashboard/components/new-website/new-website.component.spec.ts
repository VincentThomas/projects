import { InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { PasswordModule } from 'primeng/password';
import { ButtonModule } from 'primeng/button';
import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewWebsiteComponent } from './new-website.component';

describe('NewWebsiteComponent', () => {
  let component: NewWebsiteComponent;
  let fixture: ComponentFixture<NewWebsiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NewWebsiteComponent],
      imports: [
        CommonModule,
        ButtonModule,
        PasswordModule,
        ReactiveFormsModule,
        InputTextModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewWebsiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    console.log(component);
    expect(component).toBeTruthy();
  });
});
