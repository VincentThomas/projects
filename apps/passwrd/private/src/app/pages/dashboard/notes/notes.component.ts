import { Component, OnInit } from '@angular/core';
import { StorageService } from '@app/core/services/storage.service';
import { AuthService } from '@app/core/auth/auth.service';
import { Note } from '@passwrd/types';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
})
export class NotesComponent implements OnInit {
  constructor(
    private readonly storage: StorageService,
    private readonly auth: AuthService
  ) {}

  notes$: Observable<Note[]>;

  ngOnInit(): void {
    this.auth.getUser().subscribe((user) => {
      this.notes$ = this.storage.getNotes(user!.uid) as Observable<Note[]>;
    });
  }
}
