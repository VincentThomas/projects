import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { Routes } from '@angular/router';
import { IndexSettingsComponent } from './index/index-settings.component';
import { RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: '',
        component: IndexSettingsComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [SettingsComponent, IndexSettingsComponent],
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class SettingsModule {}
