import { Component, OnInit } from '@angular/core';
import {
  Auth,
  signInWithRedirect,
  GoogleAuthProvider,
} from '@angular/fire/auth';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  constructor(
    private readonly auth: Auth,
    private readonly fb: FormBuilder,
    private readonly authS: AuthService
  ) {}

  form: FormGroup;

  async ngOnInit(): Promise<void> {
    // this.form = this.fb.group({
    //   email: ['', [Validators.required, Validators.email]],
    //   password: ['', [Validators.required]],
    // });

    // const result = await getRedirectResult(this.auth);
    // if (result) {
    //   console.log(result);
    // }
    this.googleOAuth();
  }

  async emailAuth() {
    const { email, password } = this.form.value;
    await this.authS.signInWithEmailPassword(email, password);
    window.location.reload();
  }
  async emailSignUp() {
    const { email, password } = this.form.value;
    const user = await this.authS.createUserWithEmailPassword(email, password);
    console.log(user);
  }

  async googleOAuth() {
    await signInWithRedirect(this.auth, new GoogleAuthProvider());
  }
}
