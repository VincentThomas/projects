import { Component } from '@angular/core';

import { GoogleAuthProvider } from '@angular/fire/auth';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';
import { AuthService } from '@core/auth/auth.service';
import { firstValueFrom, Observable } from 'rxjs';
import { User } from '@angular/fire/auth';
import { ProfileService } from './core/http/profile.service';

@Component({
  selector: 'passwrd-private-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  items: MenuItem[] = [
    {
      label: 'Home',
      icon: 'pi pi-fw pi-home',
      routerLink: ['dashboard'],
    },
    {
      label: 'Websites',
      icon: 'pi pi-fw pi-table',
      routerLink: ['dashboard', 'w'],
    },
    {
      label: 'Notes',
      icon: 'pi pi-fw pi-file',
      routerLink: ['dashboard', 'n'],
    },
    {
      label: 'User',
      icon: 'pi pi-fw pi-user',
      items: [
        {
          label: 'Settings',
          visible: false,
          icon: 'pi pi-fw pi-cog',
          routerLink: ['settings'],
        },
        {
          label: 'Quit',
          icon: 'pi pi-fw pi-stop',
          command: () => (window.location.href = 'http://localhost:3000'),
        },
        {
          label: 'Log out',
          icon: 'pi pi-fw bx bx-log-out',
          command: async () => {
            await this.auth.signOut();
            window.location.reload();
          },
        },
      ],
    },
  ];

  user$: Observable<User>;
  pic2: string;
  constructor(
    private auth: AuthService,
    private readonly router: Router,
    private readonly profile: ProfileService
  ) {}

  async ngOnInit() {
    console.log(window.location.pathname);
    if (
      window.location.pathname === '/app' ||
      window.location.pathname === '/app/'
    ) {
      this.router.navigate(['dashboard']);
    }

    this.user$ = this.auth.getUser() as Observable<User>;
    await this.getRandomProfilePics();
  }

  async getRandomProfilePics() {
    const user = await firstValueFrom(this.auth.getUser());
    this.pic2 = this.profile.getIcon('human', user!.uid);
  }

  async login() {
    const provider = new GoogleAuthProvider();
    await this.auth.signInWithProvider(provider);
  }
}
