import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getAuth, provideAuth } from '@angular/fire/auth';
import { environment } from '../../environments/environment';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,

    provideFirebaseApp(() => {
      const { firebaseConfig } = environment;

      return initializeApp(firebaseConfig);
    }),
    provideFirestore(() => getFirestore()),
    provideAuth(() => getAuth()),
  ],
})
export class FirebaseModule {}
