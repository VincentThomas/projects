import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { Auth, user } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root',
})
export class UserIdResolver implements Resolve<string | undefined> {
  constructor(private readonly auth: Auth) {}
  async resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<string | undefined> {
    const usr = await firstValueFrom(user(this.auth));
    return usr?.uid;
  }
}
