import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

type sprite =
  | 'male'
  | 'female'
  | 'human'
  | 'identicon'
  | 'initials'
  | 'bottts'
  | 'avataaars'
  | 'jdenticon'
  | 'gridy'
  | 'micah';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  constructor(private readonly http: HttpClient) {}

  private getUrl(type: sprite, seed: string) {
    const url = environment.apiUrls.diceBear;
    return `${url}/${type}/${seed}.svg`;
  }

  public getIcon(type: sprite, seed: string) {
    return this.getUrl(type, seed);
  }
}
