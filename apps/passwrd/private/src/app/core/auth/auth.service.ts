import { Injectable } from '@angular/core';
import {
  Auth,
  signInWithEmailAndPassword,
  signInWithRedirect,
  AuthProvider,
  signOut,
  User,
  user,
  createUserWithEmailAndPassword,
} from '@angular/fire/auth';
import { firstValueFrom, map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private readonly auth: Auth) {}

  async signInWithEmailPassword(email: string, password: string) {
    return await signInWithEmailAndPassword(this.auth, email, password);
  }

  async signInWithProvider(provider: AuthProvider) {
    return await signInWithRedirect(this.auth, provider);
  }

  async signOut() {
    return await signOut(this.auth);
  }

  getUser(): Observable<User | null> {
    // firstValueFrom(user(this.auth));
    return user(this.auth);
  }

  async getUserId(): Promise<string> {
    const usr = user(this.auth).pipe(map((user) => user!.uid));
    return await firstValueFrom(usr);
  }

  async createUserWithEmailPassword(email: string, password: string) {
    return await createUserWithEmailAndPassword(this.auth, email, password);
  }
}
