import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './auth/auth.service';
import { FirebaseModule } from './firebase.module';
import { UserIdResolver } from './resolvers/user-id.resolver';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  imports: [CommonModule, FirebaseModule, HttpClientModule],
  providers: [AuthService, UserIdResolver, AuthGuard, LoginGuard],
})
export class CoreModule {}
