import { Injectable } from '@angular/core';
import {
  Firestore,
  doc,
  setDoc,
  deleteDoc,
  collection,
  collectionData,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Credential } from '@passwrd/types';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor(private readonly db: Firestore) {}

  async addWebsite(
    website: string,
    username: string,
    password: string,
    uid: string
  ) {
    const dcRef = doc(this.db, `storage/${uid}/websites/${website}`);
    return await setDoc(dcRef, { username, password });
  }

  getWebsites(uid: string): Observable<Credential[]> {
    const colRef = collection(this.db, `storage/${uid}/websites`);
    return collectionData(colRef, { idField: 'website' }) as Observable<
      Credential[]
    >;
  }

  removeWebsite(uid: string, website: string) {
    const docRef = doc(this.db, `storage/${uid}/websites/${website}`);
    deleteDoc(docRef);
  }

  async addNote(tags: string[] = [], title: string, note: string, uid: string) {
    const docRef = doc(this.db, `storage/${uid}/notes/${title}`);
    return await setDoc(docRef, { tags, note });
  }

  getNotes(uid: string) {
    const colRef = collection(this.db, `storage/${uid}/notes`);
    return collectionData(colRef, { idField: 'title' });
  }

  removeNote(uid: string, note: any) {
    note = note.title;
    const docRef = doc(this.db, `storage/${uid}/notes/${note}`);
    deleteDoc(docRef);
  }
}
