import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from './core/core.module';
import { MenubarModule } from 'primeng/menubar';
import { ShellModule } from './shell/shell.module';
import { UserIdResolver } from './core/resolvers/user-id.resolver';
import { LoginGuard } from './core/guards/login.guard';

const routes: Routes = [
  {
    path: 'dashboard',
    resolve: [UserIdResolver],
    loadChildren: async () =>
      (await import('./pages/dashboard/dashboard.module')).DashboardModule,
  },
  {
    path: 'auth',
    canActivate: [LoginGuard],
    loadChildren: async () =>
      (await import('./pages/auth/auth.module')).AuthModule,
  },

  {
    path: 'settings',

    loadChildren: async () =>
      (await import('./pages/settings/settings.module')).SettingsModule,
  },
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    RouterModule.forRoot(routes),
    MenubarModule,
    ShellModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
