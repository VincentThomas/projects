import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { DividerModule } from 'primeng/divider';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [FooterComponent],
  imports: [CommonModule, DividerModule, ButtonModule],
  exports: [FooterComponent],
})
export class ShellModule {}
