import { Component, OnInit } from '@angular/core';
import {
  Auth,
  signInWithRedirect,
  GoogleAuthProvider,
  getRedirectResult,
} from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'projects-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private readonly auth: Auth, private router: Router) {}

  async ngOnInit() {
    const result = await getRedirectResult(this.auth);
    console.log(result);
    // if (result) {
    //   window.location.href = 'http://localhost:4200';
    // }
  }

  login() {
    signInWithRedirect(this.auth, new GoogleAuthProvider());
  }
}
