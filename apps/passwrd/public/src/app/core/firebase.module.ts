import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { provideAuth, getAuth } from '@angular/fire/auth';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../../environments/environment';

const firebaseModules = [
  provideFirebaseApp(() => initializeApp(environment.firebaseConfig)),
  provideFirestore(() => getFirestore()),
  provideAuth(() => getAuth()),
];

@NgModule({
  declarations: [],
  imports: [CommonModule, ...firebaseModules],
})
export class FirebaseModule {}
