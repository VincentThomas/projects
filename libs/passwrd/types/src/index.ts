// export * from './lib/PasswrdPasswrdTypes';

export interface Credential {
  website: string;
  username: string;
  password: string;
}

export interface Note {
  title: string;
  note: string;
  tags: string[];
}
